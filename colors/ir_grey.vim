set background=dark
hi clear

if exists("syntax_on")
  syntax reset
endif

let colors_name = "ir_grey"

call extend(v:colornames, {
      \ "ir_grey_background": "#1d1d1d",
      \ "ir_grey_background_slightly_highlighted": "#2d2d2d",
      \ "ir_grey_background_light": "#44475a",
      \ "ir_grey_background_error": "#ff6c60",
      \ "ir_grey_foreground": "#f6f3e8",
      \ "ir_grey_foreground_muted": "#3d3d3d",
      \ "ir_grey_foreground_very_muted": "#070707",
      \ "ir_grey_foreground_slightly_light": "#a6a6a6",
      \ "ir_grey_foreground_light": "#ffffff",
      \ "ir_grey_foreground_highlighted": "#cae682",
      \ "ir_grey_text_comment": "#7c7c7c",
      \ "ir_grey_text_string": "#a8ff60",
      \ "ir_grey_number": "#ff73fd",
      \ "ir_grey_keyword": "#96cbfe",
      \ "ir_grey_constant": "#99cc99",
      \ "ir_grey_type": "#ffff98",
      \ "ir_grey_delimiter": "#00a0a0",
      \ "ir_grey_conditional": "#6699cc",
      \ "ir_grey_statement": "#6699cc",
      \ "ir_grey_todo": "#8f8f8f",
      \ "ir_grey_identifier": "#c6c5fe",
      \ "ir_grey_function": "#ffd2a7",
      \ "ir_grey_string_delimiter": "#336633",
      \ "ir_grey_regexp": "#b18a3d",
      \ "ir_grey_regexp_delimiter": "#ff8000",
      \ "ir_grey_special": "#e18964",
      \ })

" General colors
hi Normal           guifg=ir_grey_foreground          guibg=ir_grey_background
hi NonText          guifg=ir_grey_foreground_muted    guibg=ir_grey_background

hi Cursor           gui=reverse      cterm=reverse
hi LineNr           guifg=ir_grey_foreground_muted     guibg=ir_grey_background     gui=NONE      cterm=NONE

hi VertSplit        guifg=ir_grey_foreground_light     guibg=ir_grey_background_slightly_highlighted     gui=NONE      cterm=NONE
hi StatusLine       guifg=ir_grey_foreground_light     guibg=ir_grey_background_slightly_highlighted     gui=NONE      cterm=NONE
hi StatusLineNC     guifg=ir_grey_foreground_slightly_light     guibg=ir_grey_background_slightly_highlighted     gui=NONE      cterm=NONE

hi Folded           guifg=ir_grey_foreground_slightly_light     guibg=ir_grey_background_light     gui=NONE      cterm=NONE
hi Title            guifg=ir_grey_foreground     guibg=NONE        gui=bold      cterm=NONE
hi Visual           guifg=NONE        guibg=ir_grey_background_slightly_highlighted     gui=NONE      cterm=NONE

hi SpecialKey       guifg=#808080     guibg=#343434     gui=NONE      cterm=NONE

hi WildMenu         guifg=green       guibg=yellow      gui=NONE      cterm=NONE
hi PmenuSbar        guifg=ir_grey_background       guibg=ir_grey_foreground_light       gui=NONE      cterm=NONE
hi SignColumn       guifg=ir_grey_foreground_very_muted     guibg=ir_grey_background     gui=NONE      cterm=NONE

hi Error            guifg=NONE        guibg=NONE        gui=undercurl ctermbg=red cterm=NONE     guisp=ir_grey_background_error " undercurl color
hi ErrorMsg         guifg=ir_grey_foreground_light       guibg=ir_grey_background_error     gui=BOLD      cterm=NONE
hi WarningMsg       guifg=ir_grey_foreground_light       guibg=ir_grey_background_error     gui=BOLD      cterm=NONE

" Message displayed in lower left, such as --INSERT--
hi ModeMsg          guifg=ir_grey_foreground_slightly_light       guibg=ir_grey_background     gui=BOLD    cterm=BOLD

if version >= 700 " Vim 7.x specific colors
  hi CursorLine     guifg=NONE        guibg=ir_grey_background_slightly_highlighted     gui=BOLD     cterm=BOLD
  hi CursorLineNR   guifg=ir_grey_text_string      guibg=ir_grey_background_slightly_highlighted     gui=BOLD      cterm=BOLD
  hi CursorColumn   guifg=NONE        guibg=ir_grey_background_slightly_highlighted     gui=NONE      cterm=BOLD
  hi MatchParen     guifg=ir_grey_text_string     guibg=ir_grey_background     gui=BOLD      cterm=NONE
  hi Pmenu          guifg=ir_grey_foreground     guibg=ir_grey_background_light     gui=NONE      cterm=NONE
  hi PmenuSel       guifg=ir_grey_background     guibg=ir_grey_foreground_highlighted     gui=NONE      cterm=NONE
  hi Search         guifg=NONE        guibg=NONE        gui=underline cterm=underline
endif

" Syntax highlighting
hi Comment          guifg=ir_grey_text_comment     guibg=NONE        gui=NONE      cterm=NONE
hi String           guifg=ir_grey_text_string     guibg=NONE        gui=NONE cterm=NONE
hi Number           guifg=ir_grey_number     guibg=NONE        gui=NONE      cterm=NONE

hi Keyword          guifg=ir_grey_keyword     guibg=NONE        gui=NONE      cterm=NONE
hi PreProc          guifg=ir_grey_keyword     guibg=NONE        gui=NONE      cterm=NONE
hi Conditional      guifg=ir_grey_conditional     guibg=NONE        gui=NONE      cterm=NONE

hi Todo             guifg=ir_grey_todo     guibg=NONE        gui=NONE      cterm=NONE
hi Constant         guifg=ir_grey_constant     guibg=NONE        gui=NONE      cterm=NONE

hi Identifier       guifg=ir_grey_identifier     guibg=NONE        gui=NONE      cterm=NONE
hi Function         guifg=ir_grey_function     guibg=NONE        gui=NONE      cterm=NONE
hi Type             guifg=ir_grey_type guibg=NONE gui=NONE cterm=NONE
hi Statement        guifg=ir_grey_statement     guibg=NONE        gui=NONE      cterm=NONE

hi Special          guifg=ir_grey_special     guibg=NONE        gui=NONE      cterm=NONE
hi Delimiter        guifg=ir_grey_delimiter     guibg=NONE        gui=NONE      cterm=NONE
hi Operator         guifg=ir_grey_foreground_light       guibg=NONE        gui=NONE      cterm=NONE

hi link Character       Constant
hi link Boolean         Constant
hi link Float           Number
hi link Repeat          Statement
hi link Label           Statement
hi link Exception       Statement
hi link Include         PreProc
hi link Define          PreProc
hi link Macro           PreProc
hi link PreCondit       PreProc
hi link StorageClass    Type
hi link Structure       Type
hi link Typedef         Type
hi link Tag             Special
hi link SpecialChar     Special
hi link SpecialComment  Special
hi link Debug           Special
