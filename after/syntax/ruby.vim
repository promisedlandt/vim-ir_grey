" Regexp are sufficiently different from Strings that we want different
" highlighting for them
hi rubyRegexp          guifg=ir_grey_regexp             guibg=NONE      gui=NONE      cterm=NONE
hi rubyRegexpDelimiter guifg=ir_grey_regexp_delimiter   guibg=NONE      gui=NONE      cterm=NONE

" By default linked to Statement
hi link rubyControl Conditional

" By default linked to Delimiter, too bright imo
hi link rubyStringDelimiter Comment

" By default link to Define
hi link rubyClass  Keyword
hi link rubyModule Keyword
