" Since the theme uses variables for the colournames, we don't want to query
" the 'fg' attribute, but 'fg#', which translates the variable name to the
" colour code.
if exists("g:loaded_fzf") && !exists("g:fzf_colors")
      let g:fzf_colors = {
                        \ "fg":      ["fg#", "Normal"],
                        \ "bg":      ["bg#", "Normal"],
                        \ "hl":      ["fg#", "Comment"],
                        \ "fg+":     ["fg#", "CursorLine", "CursorColumn", "Normal"],
                        \ "bg+":     ["bg#", "CursorLine", "CursorColumn"],
                        \ "hl+":     ["fg#", "Statement"],
                        \ "info":    ["fg#", "PreProc"],
                        \ "prompt":  ["fg#", "Normal"],
                        \ "pointer": ["fg#", "Exception"],
                        \ "marker":  ["fg#", "Keyword"],
                        \ "spinner": ["fg#", "Label"],
                        \ "gutter":  ["bg#", "Normal"],
                        \ "border":  ["fg#", "Folded"],
                        \ "header":  ["fg#", "Comment"]
                        \ }
endif

highlight QuickScopePrimary   guifg=ir_grey_keyword      gui=underline cterm=underline
highlight QuickScopeSecondary guifg=ir_grey_text_comment gui=underline cterm=underline
