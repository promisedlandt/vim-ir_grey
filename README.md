## Description

High contrast (neo)vim colourscheme.

Originally based on the vim version of the Textmate IR_Black color scheme, created by Todd Werth.

## Requirements

The colourscheme uses the GUI colours only. vim can display these colours in the terminal since [7.4.1770](https://groups.google.com/g/vim_dev/c/ruRV8HGj_Sg), I'm guessing this colourscheme is useless for terminal vim versions older than that.

## Features

- Dark, but not black
- High contrast, works very well with my red/green colourblindness
- Low contrast for things you don't always want to notice (current line, line numbers other than current line, non-text characters)
- Search results are underlined, not coloured
- Includes a few custom highlight groups for Ruby

### Language specific highlights

The colourscheme makes use of some extra highlight groups from [vim-polyglot](https://github.com/sheerun/vim-polyglot) or [vim-ruby](https://github.com/vim-ruby/vim-ruby) (which is included in vim-polyglot).  
No additional configuration is necessary.

---

## Installation

Install it like any other plugin, then add the following to your config file:

```
syntax enable " enable syntax highlighting
colorscheme ir_grey
```
